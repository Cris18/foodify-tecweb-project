-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Feb 20, 2019 alle 00:50
-- Versione del server: 10.1.37-MariaDB
-- Versione PHP: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `foodifydb`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `aula`
--

CREATE TABLE `aula` (
  `idAula` smallint(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `aula`
--

INSERT INTO `aula` (`idAula`) VALUES
(1),
(2),
(3),
(4),
(5),
(6),
(7),
(8),
(9),
(10);

-- --------------------------------------------------------

--
-- Struttura della tabella `carrello`
--

CREATE TABLE `carrello` (
  `idCarrello` smallint(8) NOT NULL,
  `email` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `carrello`
--

INSERT INTO `carrello` (`idCarrello`, `email`) VALUES
(3, '123@prova.com'),
(4, 'ciao@ciao.com'),
(2, 'cri@cri.com'),
(0, 'dadelillo97@hotmail.it'),
(8, 'marco@marco.com'),
(1, 'paolo.paolillo@tper.it'),
(5, 'test1@test.com'),
(6, 'test2@gmail.com'),
(7, 'test3@gmail.com'),
(9, 'test4@test.com');

-- --------------------------------------------------------

--
-- Struttura della tabella `cliente`
--

CREATE TABLE `cliente` (
  `email` varchar(30) NOT NULL,
  `nome` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `cliente`
--

INSERT INTO `cliente` (`email`, `nome`) VALUES
('123@prova.com', 'Studente'),
('ciao@ciao.com', 'Studente'),
('cri@cri.com', 'Studente'),
('dadelillo97@hotmail.it', 'Studente'),
('marco@marco.com', 'Studente'),
('paolo.paolillo@tper.it', 'Studente'),
('test1@test.com', 'Studente'),
('test2@gmail.com', 'Studente'),
('test3@gmail.com', 'Studente'),
('test4@test.com', 'Studente');

-- --------------------------------------------------------

--
-- Struttura della tabella `fattorino`
--

CREATE TABLE `fattorino` (
  `nome` varchar(16) NOT NULL,
  `cognome` varchar(16) NOT NULL,
  `Dis_email` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `fattorino`
--

INSERT INTO `fattorino` (`nome`, `cognome`, `Dis_email`, `email`) VALUES
('Marco', 'Lucarelli', 'piadawithlove@gmail.com', 'marco.lucarelli@gmail.com');

-- --------------------------------------------------------

--
-- Struttura della tabella `fornitore`
--

CREATE TABLE `fornitore` (
  `nome` varchar(16) NOT NULL,
  `cognome` varchar(16) NOT NULL,
  `CF` varchar(16) NOT NULL,
  `P_IVA` varchar(8) NOT NULL,
  `telefono` varchar(10) NOT NULL,
  `ristorante` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `immagine` varchar(255) NOT NULL,
  `descrizione` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `fornitore`
--

INSERT INTO `fornitore` (`nome`, `cognome`, `CF`, `P_IVA`, `telefono`, `ristorante`, `email`, `immagine`, `descrizione`) VALUES
('Marco', 'Luciani', 'MCRDCJEKD14DH70B', '12345678', '3332533325', 'Wok YoYo Sushi', 'wokyoyo@gmail.com', '/foodify-tecweb-project/img/tavolo-sushi.jpg', 'Do you like Sushi? Try our Sushi and you won\'t regret!'),
('Luca', 'Semprini', 'LCCSJDJKCY7DJDIS', '87654321', '3283388332', 'Pizza Hut', 'pizzahut@gmail.com', '/foodify-tecweb-project/img/pizza-hut-logo.png', 'Pizza, Pizza and again Pizza!'),
('Franco', 'Neri', 'HFJKDUCNDJFKDLSI', '93847568', '3205654892', 'Fries Lovers', 'frieslovers@gmail.com', '/foodify-tecweb-project/img/french-fries.jpg', 'Only French Fries. That\'s it.'),
('Sergio', 'Pizarri', 'EFUHFJDJS', '3129812', '3192833838', 'Pasta da Sergio', 'pastadasergio@gmail.com', '/foodify-tecweb-project/img/pasta-seller.jpg', 'Homemade Pasta by Sergio'),
('Andrea', 'Curiosi', 'ERJERIJHKEJFWKD', '31928392', '3404849489', 'Burger Town', 'burgertown@gmail.com', '/foodify-tecweb-project/img/burger-seller.jpg', 'Burgers are like magic'),
('Marco', 'Lucia', 'MRCDJCHSKHXAJN', '38109394', '3459685478', 'Piada with Love', 'piadawithlove@gmail.com', '/foodify-tecweb-project/img/piadina-seller.jpg', 'Our Piada is made with only Love');

-- --------------------------------------------------------

--
-- Struttura della tabella `listino`
--

CREATE TABLE `listino` (
  `idListino` smallint(8) NOT NULL,
  `email` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `listino`
--

INSERT INTO `listino` (`idListino`, `email`) VALUES
(1, 'angolodigermania@gmail.com'),
(0, 'asd@asd.com'),
(4, 'wokyoyo@gmail.com'),
(5, 'pizzahut@gmail.com'),
(6, 'frieslovers@gmail.com'),
(7, 'pastadasergio@gmail.com'),
(8, 'burgertown@gmail.com'),
(9, 'piadawithlove@gmail.com');

-- --------------------------------------------------------

--
-- Struttura della tabella `notifiche`
--

CREATE TABLE `notifiche` (
  `idOrdine` smallint(8) NOT NULL,
  `forn_email` varchar(30) NOT NULL,
  `cli_email` varchar(30) NOT NULL,
  `stato` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `ordine`
--

CREATE TABLE `ordine` (
  `idOrdine` smallint(8) NOT NULL,
  `data` date NOT NULL,
  `ora` varchar(5) NOT NULL,
  `prezzo_tot` double(4,2) NOT NULL,
  `fornitore_email` varchar(30) NOT NULL,
  `cliente_email` varchar(30) NOT NULL,
  `idAula` smallint(8) NOT NULL,
  `stato` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `ordine`
--

INSERT INTO `ordine` (`idOrdine`, `data`, `ora`, `prezzo_tot`, `fornitore_email`, `cliente_email`, `idAula`, `stato`) VALUES
(0, '0000-00-00', '05:55', 10.20, 'angolodigermania@gmail.com', 'test4@test.com', 1, 'Non confermato'),
(1, '0000-00-00', '12:50', 10.20, 'angolodigermania@gmail.com', 'test4@test.com', 1, 'Non confermato'),
(2, '0000-00-00', '12:50', 10.50, 'angolodigermania@gmail.com', 'test4@test.com', 4, 'non confermato'),
(3, '0000-00-00', '07:30', 10.20, 'angolodigermania@gmail.com', 'test4@test.com', 3, 'Non confermato'),
(4, '2019-02-19', '06:30', 10.20, 'angolodigermania@gmail.com', 'test4@test.com', 1, 'Non confermato'),
(5, '2019-02-20', '12:30', 30.60, 'angolodigermania@gmail.com', 'test4@test.com', 9, 'Non confermato'),
(6, '2019-02-21', '12:50', 99.99, 'frieslovers@gmail.com', 'test4@test.com', 9, 'Non confermato');

-- --------------------------------------------------------

--
-- Struttura della tabella `prodotto`
--

CREATE TABLE `prodotto` (
  `idProdotto` smallint(8) NOT NULL,
  `immagine` varchar(100) NOT NULL,
  `nome` varchar(30) NOT NULL,
  `prezzo` double(4,2) NOT NULL,
  `tipo` varchar(30) NOT NULL,
  `descrizione` varchar(60) NOT NULL,
  `idListino` smallint(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `prodotto`
--

INSERT INTO `prodotto` (`idProdotto`, `immagine`, `nome`, `prezzo`, `tipo`, `descrizione`, `idListino`) VALUES
(1, '/foodify-tecweb-project/img/double-cheese-burger.png', 'Double Cheese Burger', 10.20, '', '', 1),
(2, 'https://img.taste.com.au/Wf8mL7LT/w720-h480-cfill-q80/taste/2016/11/jessica-39581-2.jpeg', 'Pizza', 5.80, 'pizza', 'Margherita', 1),
(3, '/foodify-tecweb-project/img/miso-soup.jpg', 'Miso Soup', 3.99, 'Vegan', 'A traditional Japanese soup consisting of a stock called Das', 4),
(4, '/foodify-tecweb-project/img/salmon-sashimi.jpg', 'Salmon Sashimi', 5.00, 'Sashimi', '8 Pieces of Salmon Sashimi', 4),
(5, '/foodify-tecweb-project/img/margherita-pizza.jpg', 'Margherita Extra Cheese', 6.00, 'Pizza', 'Extra Cheese and Tomato Sauce Pizza', 5),
(6, '/foodify-tecweb-project/img/peperoni-pizza.jpg', 'Pepperoni Cheese', 6.95, 'Pizza', 'Pepperoni and Cheese', 5),
(7, '/foodify-tecweb-project/img/french-fries-classic.jpg', 'French Fries Classic', 3.50, 'Fries', 'Classic French Fries for 2 people', 6),
(8, '/foodify-tecweb-project/img/french-fries-homemade.jpg', 'Homemade French Fries', 5.00, 'Fries', 'Homemade Flavored French Fries', 6),
(9, '/foodify-tecweb-project/img/patate-crescione.jpg', 'Potato Crescione', 5.00, 'Crescione', 'Crescione Romagnolo with Potatoes', 9),
(10, '/foodify-tecweb-project/img/mozzarella-crescione.jpg', 'Cheese Crescione', 4.50, 'Crescione', 'Tomato, Cheese Crescione', 9),
(11, '/foodify-tecweb-project/img/carbonara-pasta.jpg', 'Carbonara Pasta', 5.00, 'Pasta', 'Carbonara Pasta. Guanciale, Eggs, Pepe, Cheese', 7),
(12, '/foodify-tecweb-project/img/pesto-pasta.jpg', 'Pesto Pasta', 7.00, 'Pasta', 'Pasta with Pesto from Geneva', 7),
(13, '/foodify-tecweb-project/img/baconcheese-burger.jpg', 'Baconcheese Burger', 6.00, 'Burger', 'Bacon, Cheese, Onion Burger', 8),
(14, '/foodify-tecweb-project/img/fuego-burger.jpg', 'Fuego Burger', 8.00, 'Burger', 'Hot Pepper Burger. Really HOT.', 8);

-- --------------------------------------------------------

--
-- Struttura della tabella `prodotto_in_carrello`
--

CREATE TABLE `prodotto_in_carrello` (
  `idCarrello` smallint(8) NOT NULL,
  `idProdotto_in_carrello` smallint(8) NOT NULL,
  `porzioni` smallint(4) NOT NULL,
  `prezzo_tot` double(4,2) NOT NULL,
  `idProdotto` smallint(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `prodotto_ordinato`
--

CREATE TABLE `prodotto_ordinato` (
  `idProdotto_ordinato` smallint(8) NOT NULL,
  `porzioni` smallint(4) NOT NULL,
  `prezzo_tot` double(4,2) NOT NULL,
  `idProdotto` smallint(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `session`
--

CREATE TABLE `session` (
  `email` varchar(30) NOT NULL,
  `tipo` varchar(20) NOT NULL,
  `nome` varchar(20) NOT NULL,
  `forn_email` varchar(30) NOT NULL,
  `IDLISTINO` varchar(30) DEFAULT NULL,
  `IDCARRELLO` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `session`
--

INSERT INTO `session` (`email`, `tipo`, `nome`, `forn_email`, `IDLISTINO`, `IDCARRELLO`) VALUES
('123@prova.com', 'Studente', 'Studente', '', '', '3'),
('admin@admin.com', 'admin', 'amministratore', '', '', ''),
('angolodigermania@gmail.com', 'Ristorante', 'roberto', '', '4', ''),
('asd@asd.com', 'Ristorante', 'asd', '', '0', ''),
('ciao@ciao.com', 'Studente', 'Studente', '', '', '4'),
('cri@cri.com', 'Studente', 'Studente', '', '', '2'),
('dadelillo97@hotmail.it', 'Studente', 'Studente', '', '', '0'),
('marco@marco.com', 'Studente', 'Studente', '', '', '8'),
('paolo.paolillo@tper.it', 'Studente', 'Studente', '', '', '1'),
('test4@test.com', 'Studente', 'Studente', '', '', '9'),
('test4@test.com', 'Studente', 'Studente', '', '', '9'),
('test4@test.com', 'Studente', 'Studente', '', '', '9'),
('piadawithlove@gmail.com', 'Ristorante', 'Marco', '', '9', ''),
('pastadasergio@gmail.com', 'Ristorante', 'Sergio', '', '7', ''),
('burgertown@gmail.com', 'Ristorante', 'Andrea', '', '8', ''),
('test4@test.com', 'Studente', 'Studente', '', '', '9'),
('test4@test.com', 'Studente', 'Studente', '', '', '9'),
('marco.lucarelli@gmail.com', 'Delivery-Man', 'Marco', 'piadawithlove@gmail.com', '', ''),
('test4@test.com', 'Studente', 'Studente', '', '', '9');

-- --------------------------------------------------------

--
-- Struttura della tabella `utente`
--

CREATE TABLE `utente` (
  `email` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `FATTORINO` varchar(30) DEFAULT NULL,
  `FORNITORE` varchar(30) DEFAULT NULL,
  `CLIENTE` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `utente`
--

INSERT INTO `utente` (`email`, `password`, `FATTORINO`, `FORNITORE`, `CLIENTE`) VALUES
('a@a.com', '$2y$10$/bWSTlkTut22VfrPZTbouuTfNsAoh6uc.3Pxb5DSt8WlnZUVbsItu', '', '', 'Studente'),
('admin@admin.com', '$2y$10$21gNaHydbCTN5u7gOVDEOe3v56JGTbRYb/uu9NmKm/7ivEAvm1AlW', '', '', ''),
('asd@asd.com', '$2y$10$L7tuWuXb3gRUhhjX1Re8A.qdgg5FjfpsKAIEvkjPbz8CdIti96nOS', '', 'Ristorante', ''),
('dadelillo97@hotmail.it', '$2y$10$Qvj2EQ7Z.5TtuGoj.KHKvOwjRHKjVQ27bm/S6VQAnU3G.wOSf06D2', '', '', 'Studente'),
('paolo.paolillo@uso.it', '$2y$10$omVzAnyvmT7izXri9oWrZeBcB2ZjQ8Uw/4IcAW4PPMHB8pk3XpRhi', 'Delivery-Man', '', ''),
('marco@marco.com', '$2y$10$.sVinbsjcMizngq1WtP7k.jrCknAdBlI8mC2WpwPeLpSlu7BstYza', '', '', 'Studente'),
('angolo@angolo.it', '$2y$10$v/xyB25ATCr537tAw0oYMOWE1s7WxwbOnKt6Eij7niI6PRu/DRGuC', '', 'Ristorante', ''),
('angolodigermania@gmail.com', '$2y$10$vYYRpNeXFQHxQFmIkYUC7eOca.DXROsMHRx.udKQYE1BD16fhmJH6', '', 'Ristorante', ''),
('test4@test.com', '$2y$10$8fTxDkgGCoDgAjEM1Xog3en6men9d.fbit9.svdQgn3im/oCvgoOe', '', '', 'Studente'),
('pastadasergio@gmail.com', '$2y$10$BOC5YQW1IaLjyWRi6VgO5ek.oCccH8DHLBDsIL7FgiktYcd9m9ma.', '', 'Ristorante', ''),
('burgertown@gmail.com', '$2y$10$jjSUkiLhVKSqNhuuTmC4J.d6O8UlEUS9GFC8enhWoEhfryTRaaxEy', '', 'Ristorante', ''),
('piadawithlove@gmail.com', '$2y$10$n2nMxppbph5G8NK8a7JxGOGwBbhZRqiD7ITdl6yDuCEiYfDjABpWm', '', 'Ristorante', ''),
('marco.lucarelli@gmail.com', '$2y$10$xoOV0vU6J/cYWaTOopzW6u56BNglXpRQ.t9gDlK5vm3JJNSd7iO4e', 'Delivery-Man', '', '');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
