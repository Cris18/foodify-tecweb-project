<?php
session_start();
$servername="localhost";
$username ="root";
$password ="";
$database = "foodifydb";
//Creo gli array necessari
$products = array();
$products_in_cart = array();
$final_product = array();

$conn = new mysqli($servername, $username, $password, $database);
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	}
  $stmt = $conn->prepare("SELECT * FROM prodotto_in_carrello WHERE idCarrello = ?");
  $stmt->bind_param("i", $_SESSION["idCarrello"]);
  $stmt->execute();
    $result = $stmt->get_result();
    if($result->num_rows > 0){
      $output = array();
      while($row = $result->fetch_assoc()) {
        $output[] = $row;
      }
	  foreach ($output as $key => $value) {
      $prodotto = array();
			$prodotto['idProdotto'] = $output[$key]['idProdotto'];
			$prodotto['porzioni'] = $output[$key]['porzioni'];
      $prodotto['prezzo_tot'] = $output[$key]['prezzo_tot'];
      $products_in_cart[$key] = $prodotto;
    }
    $stmt->close();

		foreach ($products_in_cart as $key => $value) {
			$stmt = $conn->prepare("SELECT * FROM prodotto WHERE idProdotto = ?");
		  $stmt->bind_param("i", $products_in_cart[$key]['idProdotto']);
		  $stmt->execute();
		    $result = $stmt->get_result();
		    if($result->num_rows > 0){
		      $output = array();
		      while($row = $result->fetch_assoc()) {
		        $output[] = $row;
		      }
		    }
		    foreach ($output as $key => $value) {
		      $prodotto = array();
					$prodotto['idProdotto'] = $output[$key]['idProdotto'];
					$prodotto['immagine'] = $output[$key]['immagine'];
		      $prodotto['nome'] = $output[$key]['nome'];
					$prodotto['tipo'] = $output[$key]['tipo'];
		      $prodotto['descrizione'] = $output[$key]['descrizione'];
		      array_push($products, $prodotto);
		    }
		    $stmt->close();
		}
				foreach ($products_in_cart as $key => $value) {
					$prodotto = array();
					$prodotto['idProdotto'] = $products[$key]['idProdotto'];
					$prodotto['immagine'] = $products[$key]['immagine'];
		      $prodotto['nome'] = $products[$key]['nome'];
					$prodotto['tipo'] = $products[$key]['tipo'];
		      $prodotto['descrizione'] = $products[$key]['descrizione'];
					$prodotto['porzioni'] = $products_in_cart[$key]['porzioni'];
		      $prodotto['prezzo_tot'] = $products_in_cart[$key]['prezzo_tot'];
					array_push($final_product, $prodotto);
				}
				echo json_encode($final_product);
    }
  //Invio query
  //Chiudo connessione al DB
  $conn->close();
?>
