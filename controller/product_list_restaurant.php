<?php
session_start();
$servername = "localhost";
$username = "root";
$password = "";
$database = "foodifydb";
$products = array();
$conn = new mysqli($servername, $username, $password, $database);
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	}
  $stmt = $conn->prepare("SELECT * FROM prodotto WHERE idListino = ?");
  $stmt->bind_param("i", $_SESSION["idListino"]);
  $stmt->execute();
    $result = $stmt->get_result();

    $output = array();
    while($row = $result->fetch_assoc()) {
      $output[] = $row;
    }
    foreach ($output as $key => $value) {
      $prodotto = array();
			$prodotto['idProdotto'] = $output[$key]['idProdotto'];
			$prodotto['immagine'] = $output[$key]['immagine'];
      $prodotto['nome'] = $output[$key]['nome'];
      $prodotto['prezzo'] = $output[$key]['prezzo'];
      $prodotto['tipo'] = $output[$key]['tipo'];
      $prodotto['descrizione'] = $output[$key]['descrizione'];
      $products[$key] = $prodotto;
    }
    echo json_encode($products);
    $stmt->close();
  //Invio query
  //Chiudo la connessione con il DB
  $conn->close();
//This controller is needed for the complete list of the products that a restaurant sells.
?>
