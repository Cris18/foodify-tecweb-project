<?php
$servername = "localhost";
$username = "root";
$password = "";
$database = "foodifydb";
$conn = new mysqli($servername, $username, $password, $database);
if ($conn->connect_error) {
	die("Connection with the DB failed: " . $conn->connect_error);
}
$stmt = $conn->prepare("DELETE FROM ordine WHERE idOrdine = ?");
$stmt->bind_param("i", $_POST['idd']);
if($stmt->execute()){
  echo "Ok, Done.";
}
$stmt->close();
$conn->close();
header('Location: /foodify-tecweb-project/src/fattorino.php');
//Invio query e chiuedo la connessione con il DB. Prima di modificare
//chiedi a Cri.

//This controller handles the deletion of a product from the DB
?>
