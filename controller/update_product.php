<?php
$servername = "localhost";
$username = "root";
$password = "";
$database = "foodifydb";

$conn = new mysqli($servername, $username, $password, $database);
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}
$stmt = $conn->prepare("UPDATE prodotto SET immagine = ?, nome = ?, prezzo = ?, tipo = ?,
  descrizione = ? WHERE idProdotto = ?");
  $stmt->bind_param("ssdssi", $_POST['immagine'], $_POST['nome'], $_POST['prezzo'],
  $_POST['tipo'], $_POST['descrizione'], $_POST['id']);
  if($stmt->execute()){
    echo "Ok, Done!";
  } else {
    echo "We got a problem! Error!"
  }
  $stmt->close();
  $conn->close();
//Questo controller gestisce l'update e quindi la modifica di un prodotto da parte del ristorante
?>
