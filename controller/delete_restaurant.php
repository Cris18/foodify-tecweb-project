<?php
$servername = "localhost";
$username = "root";
$password = "";
$database = "foodifydb";
$conn = new mysqli($servername, $username, $password, $database);
if ($conn->connect_error) {
	die("Connection with the DB failed: " . $conn->connect_error);
}
$mail = $_POST['id'];
$stmt = $conn->prepare("DELETE FROM utente WHERE email = ?");
$stmt->bind_param("s", $mail);
if($stmt->execute()){
  echo "Ok, Done.";
}
$stmt->close();
$stmt = $conn->prepare("DELETE FROM session WHERE email = ?");
$stmt->bind_param("s", $mail);
if($stmt->execute()){
  echo "Ok, Done.";
}
$stmt->close();
$stmt = $conn->prepare("DELETE FROM fornitore WHERE email = ?");
$stmt->bind_param("s", $mail);
if($stmt->execute()){
  echo "Ok, Done.";
}
$stmt->close();
$conn->close();
header('Location: /foodify-tecweb-project/src/admin.php');
//Invio query e chiuedo la connessione con il DB. Prima di modificare
//chiedi a Cri.
//This controller handles the deletion of a product from the DB
?>
