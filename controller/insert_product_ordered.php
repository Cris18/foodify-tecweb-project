<?php
session_start();
$servername="localhost";
$username ="root";
$password ="";
$database = "foodifydb";

$conn = new mysqli($servername, $username, $password, $database);
  //Controllo connessione
  if ($conn->connect_errno) {
      echo "Failed to connect to MySQL: (" . $conn->connect_errno . ") " . $conn->connect_error;
  }

$stmt = $conn->prepare("INSERT INTO prodotto_ordinato VALUES (?, ?, ?, ?, ?, ?)");
$stmt->bind_param('iidiss', $_POST['idProdotto_ordinato'],
  $_POST['porzioni'], $_POST['prezzo'], $_POST['idProdotto'], $_POST['nome'], $_POST['descrizione']);
  if ($stmt->execute()) {
        echo "Ok, done!";
  }
  else{
    echo $stmt->execute();
  }
  //Chiudo la connessione con il DB
  $stmt->close();
  $conn->close();
?>
