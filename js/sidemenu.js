/*To Control the width of the sidemenu*/
var isOpen = false;
var isMobile = false;

if (window.matchMedia('(max-width: 576px)').matches) {
  isMobile = true;
}

function openNav(){
  if(isMobile===false){
    $("body").css({"position": "absolute", "overflow": "auto", "margin-left" : "15%", "width" : "85%", "transition": "0.5s"});
    document.getElementById("sidemenu").style.width = "15%";
  } else {
    document.getElementById("sidemenu").style.width = "125px";
  }
}

function closeNav(){
  if (isMobile===false){
    $("body").css({"position": "auto", "overflow-x": "hidden", "margin-left" : "0", "width" : "100%", "transition": "0.5s"});
  }
  document.getElementById("sidemenu").style.width = "0px";
}

function toggleNav(){
  if (isOpen) {
    closeNav();
    isOpen = false;
  } else {
    openNav();
    isOpen = true;
  }

/*
  function openNav() {
    $("body").css({"position": "absolute", "overflow": "auto", "margin-right" : "20%", "width" : "80%", "transition": "0.5s"});
    document.getElementById("menu-cart").style.width = "20%";
  }

  function closeNav() {
    $("body").css({"position": "auto", "overflow-x": "hidden", "margin-right" : "0", "width" : "100%", "transition": "0.5s"});
    document.getElementById("menu-cart").style.width = "0";
  }
*/
}
