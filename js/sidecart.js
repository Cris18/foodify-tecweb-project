$(document).ready(function() {
var email = new URL(window.location.href).searchParams.get("email");
var cart = [];
var seller = [];
var isCartOpen = false;
var isMobile = false;


if (window.matchMedia('(max-width: 576px)').matches) {
  isMobile = true;
}

function updateCart(){
  $.ajax({
    url: "../controller/products_in_cart.php",
    type: "POST",
    success: function(response){
      if (response.length > 0){
        var json = JSON.parse(response);
        for(var i = 0; i < json.length; i++){
          cart[i] = json[i];
        }
        populateCart();
      }
    }
  });
}

function populateCart() {
  $("#cart-products").html("");
  for(var i=0; i < cart.length; i++){
    var div = $('<div class="cart-products container-fluid"></div>');
    var divbody = $('<div class="cart-card-body container-fluid"></div>');
    var title = $("<h5 class='h5'>"+ cart[i].nome+"</h5>");
    var portions = $('<p class="text-muted ml-auto" style="border-bottom: 0.5px solid grey">x'+cart[i].porzioni+'</p>');
    var row_1 = $('<div></div>');
    var row_2 = $('<div></div>');
    var price = $("<span class='font-weight-bold'>Price:"+ " " + cart[i].prezzo_tot+"€"+"</span>");

    row_1.append(title);
    row_1.append(price);
    row_2.append(portions);
    divbody.append(row_1);
    divbody.append(row_2);
    div.append(divbody);
      $("#cart-products").append(div);
  }
}

function openCart(){
  updateCart();
  if(isMobile===false){
    $("body").css({"position": "absolute", "overflow": "auto", "margin-right" : "30%", "width" : "70%", "transition": "0.5s"});
    document.getElementById("sidecart").style.width = "30%";
  } else {
    document.getElementById("sidecart").style.width = "250px";
  }
}

function closeCart(){
  if (isMobile===false){
    $("body").css({"position": "auto", "overflow-x": "hidden", "margin-right" : "0", "width" : "100%", "transition": "0.5s"});
  }
  document.getElementById("sidecart").style.width = "0px";
}

function toggleCart() {
  if (isCartOpen) {
    closeCart();
    isCartOpen = false;
  } else {
    openCart();
    isCartOpen = true;
  }
}

$("#cart-button").on('click', function(){
  toggleCart();
});


$('#delete-cart').on('click', function(){
  $.ajax({
    url: "../controller/delete_product_from_cart.php",
    type: "POST",
    success: function(){
      location.reload();
    }
  });
});

$('#checkout-cart').on('click', function(){
    if(cart.length > 0){
      window.location.href = ("../src/checkout.php?email=" + email);
    } else {
      //alert("Your Cart is empty")
    }
});

});
