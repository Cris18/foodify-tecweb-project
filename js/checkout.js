$(document).ready(function() {
  var url = new URL(window.location.href);
  var forn_email = url.searchParams.get("email");
  var aule = [];
  var cart = [];
  var maxIdOrdine;

  $.ajax({
    url: "/foodify-tecweb-project/controller/products_in_cart.php",
    type: "POST",
    async:false,
  }).done(function(response) {
    if(response.length>0){
      var json = JSON.parse(response);
      for(var i = 0; i < json.length; i++){
        cart[i] = json[i];
      }
      getAule();
      populateProducts();
      //populateCheckoutPanel();
      getMaxIdOrder();
    }

  });

  function populateCheckoutPanel(aule) {
    var auleOptions = [];
    aule.forEach(function(item, index) {
      var option = $('<option>' + item.idAula + '</option>');
      auleOptions.push(option);
    });
    console.log("aule" + aule);
    console.log("options" + auleOptions);
    $('.choose-place').find('select').append(auleOptions);
  }

  function getAule() {

    $.ajax({
      async:false,
      url: "/foodify-tecweb-project/controller/aule.php",
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert(XMLHttpRequest + textStatus + errorThrown);
      },
      success: function(response){
        console.log(response);
        //if (response > 0) {
          var json = JSON.parse(response);
          console.log(json + "json");
          for(var i = 0; i < json.length; i++){
            aule[i] = json[i];
          }
          populateCheckoutPanel(aule);
        //}
      },
    });


  }

  function getMaxIdOrder() {
    $.ajax({
      url: "/foodify-tecweb-project/controller/max_id_order.php",
      success: function(response){
        maxIdOrdine = response;
      }
    });
  }

  function insertOrder() {
    //var metodo = $('.payment input:checked').val();
    // if(metodo != undefined){
    var aula = $('.choose-place option:selected').val();
    var ora = $('.choose-hour input').val();
    var data = $('#datepicker').val();
    if(ora != undefined && data!=undefined && data != "" && ora != "" && cart.length>0){
      var prezzo_tot = 0;
      for(var i = 0; i < cart.length; i++){
        prezzo_tot = (cart[i].porzioni * cart[i].prezzo_tot) + prezzo_tot;
      }
      $.ajax({
        url: "/foodify-tecweb-project/controller/delete_product_from_cart.php",
        type: "POST",
      });
      $.ajax({
        url: "/foodify-tecweb-project/controller/insert_order.php",
        type: "POST",
        data: {
          "idOrdine" : parseInt(maxIdOrdine), "fornitore_email" : forn_email, "prezzo_tot" : prezzo_tot, "ora" : ora, "data" : data, "idAula": aula
        },
        success: function(response){
          console.log(response);
        }, error: function(XMLHttpRequest, textStatus, errorThrown) {
          alert(XMLHttpRequest + textStatus + errorThrown);
        },
      });

      for(var i = 0; i < cart.length; i++){
        $.ajax({
          url: "/foodify-tecweb-project/controller/insert_product_ordered.php",
          type: "POST",
          data: {
            "idProdotto_ordinato" : parseInt(maxIdOrdine), "forn_email": forn_email, "idProdotto": cart[i].idProdotto, "porzioni": cart[i].porzioni,
            "prezzo": cart[i].prezzo_tot, "nome" : cart[i].nome, "descrizione" : cart[i].descrizione
          }
        });
      }

    } //if
    else if(cart.length == 0){
      //alert("Your Cart is empty");
    }
    else{
      //alert("Please Choose Date and Hour");
    }
    //}
    //window.location.replace("/foodify-tecweb-project/src/registered_homepage.php");
  }

  function populateProducts() {
    var totalprice = 0;
    var productsElements = [];
    cart.forEach(function(item, index) {
      var productItem = $('<div class="product-item"></div>');
      var nome = $('<div class="product-name" style="font-size: 18px; font-weight: bold; margin-top: 10px;">' + item.nome + '</div>');
      /*var descrizione = $('<div class="product-description">' + item.descrizione + '</div>');*/
      var prezzo = $('<div class="product-price" style="border-bottom: 0.5px solid black; margin-bottom: 15px"> Price: <span style="font-weight: bold">' + item.prezzo_tot + '€</span></div>');
      /*var tipo = $('<div class="product-type">Tipo: ' + item.tipo + '</div>');*/
      /*var immagine = $('<div class="product-image"><img src="' + item.immagine + '" alt = "product image"></div>');*/
      var quantita = $('<div class="product-counter">Quantity: <span style="font-weight: bold">' + item.porzioni + '</span></div>');
      totalprice += item.prezzo_tot;
      productItem.append([nome, quantita, prezzo]);
      productsElements.push(productItem);
    });
    $('.cart-total-price').append("<span style='margin-left: 5px; font-weight: bold; font-size: 18px;'>Total Price: " +totalprice.toFixed(2).toString()+ " €");
    $('.products-container').append(productsElements);

  }



  $('#place-order-button-card').on('click', function() {
    //alert("The payment on this website is in Demo mode. So the payment isn't required.");
    insertOrder();
  });

  $('#place-order-button-paypal').on('click', function() {
    //alert("The payment on this website is in Demo mode. So the payment isn't required.");
    insertOrder();
  });

  $('#place-order-button-cash').on('click', function() {
    //alert("The payment on this website is in Demo mode. So the payment isn't required.");
    insertOrder();
  });

  $('#datepicker').datepicker({
    minDate: '-0',
    maxDate: '+15d',
    dateFormat: 'yy-mm-dd'
  });

  /*
  $(function() {
  $("#datepicker").datepicker();
} );
*/
});
