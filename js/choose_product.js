$(document).ready(function() {
  var url = new URL(window.location.href);
  var email_selected = url.searchParams.get("email");
  var products = [];
  getProductList();

  function getProductList() {
    $.ajax({
      url: "../controller/product_list_client.php",
      type: "POST",
      data: {"email" : email_selected},
      success: function(response){

        var json = JSON.parse(response);
        for(var i = 0; i < json.length; i++){
          products[i] = json[i];
        }
        populateProducts();
        eventListeners();
      }
    });
  }

  function eventListeners() {
    $('.product-button').on('click', function() {
      /*if(email_selected != email){
        $.ajax({
          url: "../controller/delete_product_from_cart.php",
          type: "POST",
          success: function(){

          }
        });
      }*/
      console.log("Qui ci arriva");
	  var index = $(this).parent().index()-1;
    var bottone = $(this);
      var product_clicked = products[$(this).parent().index()-1];
      var quantità = $('.product-counter:eq('+index+')').val();
	  var prezzo = products[index].prezzo * $('.product-counter:eq('+index+')').val();
	  if(quantità != 0 && quantità != undefined){
		   $.ajax({
        url: "/foodify-tecweb-project/controller/insert_product_into_cart.php",
        type: "POST",
        data: {
          "id" : product_clicked.id,
          "prezzo" : prezzo,
          "quantità" : quantità,
        },
        success: function(response){
          var html = "<div class='alert alert-success'>" + response + "</div><br/>";
          bottone.after(html);
			//alert("Product added to the cart");
      //function reload(){
        //location.reload();
      //}
      //setTimeout(reload, 1000);
    }
      });
	  }
    /*
    else{
		  alert ("Please choose the quantity");
	  }
    */
    });

    $('.product-counter').on('click', function(event) {
      var standardPrice = products[$(this).parent().parent().index()-1].prezzo;
      $(event.currentTarget).parent().find('.product-price').text(standardPrice * $(event.currentTarget).val() + '€');
    });
  }

  function populateProducts() {

    var productsElements = [];

    products.forEach(function(item, index) {
      var productItem = $('<div class="product-item"></div>');
      var nome = $('<div class="product-name" style="font-weight: bold">' + item.nome + '</div>');
      var descrizione = $('<div class="product-description">' + item.descrizione + '</div>');
      var prezzo = $('<div class="product-price">' + item.prezzo + ' €</div>');
      var immagine = $('<div class="product-image"><img src="' + item.immagine + '" alt = "image of the product" width=200></div>');
      var quantita = $('<label>Quantity: <input class="product-counter" type="number" min="1" value="1"></label>');
      var button = $('<button class="product-button btn btn-primary" type="button" style="margin-left: 5px; margin-bottom: 5px">Add to Cart</button>');
      button.data('productSelected', item.id);

      productItem.append([immagine, nome, descrizione, prezzo, quantita, button]);
      productsElements.push(productItem);
    });

    $('.products-container').append(productsElements);

  }

  $('#delete-cart').on('click', function() {
	  $.ajax({
		  url: "../controller/delete_product_from_cart.php",
		  type: "POST",
		  success: function(){
			  location.reload();
		  }
      });

  });

});
