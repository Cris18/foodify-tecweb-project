<?php

$servername="localhost";
$username ="root";
$password ="";
$database = "foodifydb";


$empty = "";

$conn =new mysqli($servername, $username, $password, $database);
if ($conn->connect_errno) {
    echo "Failed to connect to MySQL: (" . $conn->connect_errno . ") " . $conn->connect_error;
}

	if($_COOKIE['user'] == ""){
		header('Location: /foodify-tecweb-project/src/guest_homepage.php');
	}
	else{
		$stmt = $conn->prepare("SELECT * FROM session WHERE email = ?");
        $stmt->bind_param("s", $_COOKIE['user']);
        $stmt->execute();
        $result = $stmt->get_result();
        if($result->num_rows > 0){
          while($row = $result->fetch_assoc()) {
			if($row['tipo'] == "Ristorante"){
				$_SESSION["email"] = $row["email"];
				$_SESSION["tipo"] = $row["tipo"];
				$_SESSION["nome"] = $row["nome"];
				$_SESSION["idListino"] = $row["IDLISTINO"];
				header('Location: /foodify-tecweb-project/src/ristoratore.php');
			}
			else if($row['tipo'] == "Studente"){
				$_SESSION["email"] = $row["email"];
				$_SESSION["tipo"] = $row["tipo"];
				$_SESSION["nome"] = $row["nome"];
				$_SESSION["idCliente"] = $row["IDCLIENTE"];
				header('Location: /foodify-tecweb-project/src/registered_homepage.php');
			}
			else if($row['tipo'] == "Delivery-Man"){
				$_SESSION["email"] = $row["email"];
				$_SESSION["tipo"] = $row["tipo"];
				$_SESSION["nome"] = $row["nome"];
				$_SESSION["email_forn"] = $row["Dis_email"];
				header('Location: /foodify-tecweb-project/src/fattorino.php');
			}
			else{
				$_SESSION["email"] = $row["email"];
				$_SESSION["tipo"] = $row["tipo"];
				$_SESSION["nome"] = $row["nome"];
				header('Location: /foodify-tecweb-project/src/admin.php');
			}
		  }
		} else {
			header('Location: /foodify-tecweb-project/src/guest_homepage.php');
		}
        $stmt->close();
	}
	exit;
?>
