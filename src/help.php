<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
  <!-- My CSS -->
  <link rel="stylesheet" href="/foodify-tecweb-project/css/help.css">
  <!-- FontAwesome Icons -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

  <!-- Google's Material Design Icons -->
  <link rel="stylesheet" href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

  <title>Foodify - Help</title>
</head>
<body>
  <div class="menu">
    <?php require 'navbar.php'; ?>
  </div>

  <div class="container-fluid" id="how-it-works" style="text-align: center">
    <h2>Help - F.A.Q</h2>
    <br>
    <h5>Are you already registered?</h5>
    <p>If you aren't already registered, go register right now! It only takes few clicks!</p>
    <br>
    <h5>How to order some food?</h5>
    <p>Order your favourite food in a few clicks! Just Login and click on the "Order Now" button to check all available Restaurants!</p>
    <br>
    <h5>How Can I Pay?</h5>
    <p>We accept all the major payment methods such as Debit or Credit Card, PayPal or Cash on Delivery!</p>
    <div class="payment-methods-list" style="text-align: center">
      <i class="fab fa-paypal" style="font-size: 30px;"></i>
      <i class="fab fa-cc-visa" style="font-size: 30px;"></i>
      <i class="fab fa-cc-mastercard" style="font-size: 30px;"></i>
      <i class="far fa-credit-card" style="font-size: 30px; color:black"
      <i class="fas fa-money-bill" style="font-size: 30px;"></i>
    </div>
  </div>
  <br>
  <br>
  <div class="container" id="footer-homepage">
    <?php require 'footer.php'; ?>
  </div>
  <!-- Google Fonts API -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Staatliches">

</body>
</html>
