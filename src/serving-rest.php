<?php
session_start();
$servername = "localhost";
$username = "root";
$password = "";
$database = "foodifydb";
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <style>

    hr {
      width: 50%;
      margin-top: 8%;
    }

    div.toast-header > * {
      text-align: center;
    }

    body{
      font-family: sans-serif;
      text-decoration: none;
    }

    div.notify{
      width: 70%;
      border: 1px solid darkgrey;
      border-radius: 8px;
      box-shadow: 3px 4px 5px darkorange;
      margin-top: 40px;
      margin-left: 110px;
      margin-bottom: 240px;
    }

    div.notify > *{
      margin: 10px 20px;
    }

    img{
      width: 30px;
    }

    button{
      border-color: white;
      font-weight: bold;
      height: 20px;
      width: 40px;
      float: right;
    }

    div.toast-body p{
      font-size: 170%;
      text-align: left;
    }

    strong{
      font-size: 350%;
      color: black;
    }

    img {
      width: 10%;
      height: 10%;
    }

    i.material-icons {
      font-size: 260%;
    }

    div.toast-body p:hover {
      font-size: 200%;
    }
    </style>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    <!-- Google's Material Design Icons -->
    <link rel="stylesheet" href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <title>Servinghome</title>
  </head>
  <body>
    <?php $conn = new mysqli($servername, $username, $password, $database);
    if ($conn->connect_error) {
    		die("Connection to the DB failed: " . $conn->connect_error);
    	} else { ?>
    <div class="container-fluid" id="back_arrow">
        <div class="nav-item" style="float:left">
          <br>
          <span onclick="history.go(-1);"><a class="navbar-menu"><i class="material-icons" style="color:black; width: 150%; height: 150%;">arrow_back</i></a></span>
        </div>
    </div>
    <div class="notify">
      <div class="toast-header">
        <img src="/foodify-tecweb-project/img/notifica.png" class="rounded mr-2" alt="Serving checked">
        <strong class="mr-auto" >Notifications</strong>
      </div>
      <?php  $stato = "Non confermato";
        $stmt = $conn->prepare("SELECT * FROM ordine WHERE fornitore_email = ? AND stato = ?");
        $stmt -> bind_param("ss", $_SESSION["email"], $stato);
        $stmt->execute();
        $result = $stmt->get_result();
        if($result->num_rows > 0){
          while($row = $result -> fetch_assoc()) {?>
            <div class="toast-body">
              <p>Effettuato ordine con identificativo : <?php echo $row["idOrdine"] ?>, arrivera' in data <?php echo $row["data"] ?> alle ore <?php echo $row["ora"] ?> nell' aula <?php echo $row["idAula"]?> all' utente <?php echo $row["cliente_email"] ?>.</p>
              <hr noshade style="margin-top: 8%;">
            </div>
    <?php }
        }
        $stmt->close();
      }?>
    </div>
    <!--
    <div class="container" id="footer-homepage">
    </div>
  -->
  </body>
</html>
