<?php
session_start();
$servername = "localhost";
$username = "root";
$password = "";
$database = "foodifydb";
?>
<!doctype html>
<html lang="en">
<head>
  <style>
  #snackbar {
    visibility: hidden;
    min-width: 250px;
    margin-left: -125px;
    background-color: #333;
    color: #fff;
    text-align: center;
    border-radius: 2px;
    padding: 16px;
    position: fixed;
    z-index: 1;
    left: 50%;
    bottom: 30px;
    font-size: 17px;
  }

  #snackbar.show {
    visibility: visible;
    -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
    animation: fadein 0.5s, fadeout 0.5s 2.5s;
  }

  @-webkit-keyframes fadein {
    from {bottom: 0; opacity: 0;}
    to {bottom: 30px; opacity: 1;}
  }

  @keyframes fadein {
    from {bottom: 0; opacity: 0;}
    to {bottom: 30px; opacity: 1;}
  }

  @-webkit-keyframes fadeout {
    from {bottom: 30px; opacity: 1;}
    to {bottom: 0; opacity: 0;}
  }

  @keyframes fadeout {
    from {bottom: 30px; opacity: 1;}
    to {bottom: 0; opacity: 0;}
  }
</style>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
<!-- My CSS -->
<link rel="stylesheet" href="/foodify-tecweb-project/css/style.css">
<!-- FontAwesome Icons -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
<!-- Google's Material Design Icons -->
<link rel="stylesheet" href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<title>Foodify - HomePage</title>
</head>
<body>
  <div class="menu">
    <?php require 'registered-user-navbar.php';?>
  </div>
  <!-- CARUSEL ITEM (photo) -->
  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active" id="item1">
        <img src="/foodify-tecweb-project/img/pasta2.jpg" class="d-block w-100 img-fluid" style="text-align: center" alt="Pasta">
        <div class="carousel-caption d-none d-md-block">
          <h3>PASTA</h3>
          <p>The best Italian Pasta delivered to you</p>
        </div>
      </div>
      <div class="carousel-item" id="item2">
        <img src="/foodify-tecweb-project/img/hamburger2.jpg" class="d-block w-100 img-fluid" style="text-align: center" alt="Hamburger">
        <div class="carousel-caption d-none d-md-block">
          <h3>HAMBURGERS</h3>
          <p>The best Cesena's Hamburgers</p>
        </div>
      </div>
      <div class="carousel-item" id="item3">
        <img src="/foodify-tecweb-project/img/piadina2.jpg" class="d-block w-100 img-fluid" style="text-align: center" alt="Piadina Romagnola">
        <div class="carousel-caption d-none d-md-block">
          <h3>PIADINA Romagnola</h3>
          <p>Most spicy and fresh Piadina</p>
        </div>
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>

  <div class="free-delivery-food" style="text-align: center">
    <p><span id="free-delivery-color-change">Free Delivery,</span><br> always!</p>
  </div>

  <div class="order-now-button" style="text-align: center">
    <a href="/foodify-tecweb-project/src/all_restaurants.php"><img src="/foodify-tecweb-project/img/OrderNow.png" class="img-fluid" alt="Order Now"></a>
  </div>

  <div class="trust-icons" style="text-align: center">
    <i class="material-icons" style="color:black; font-size:75px;">fastfood</i>
    <h5>Choose Your Restaurant</h5>
    <i class="material-icons" style="color:black; font-size:75px;">attach_money</i>
    <h5>Pay with PayPal, Card or Cash On Delivery</h5>
    <i class="material-icons" style="color:black; font-size:75px;">local_shipping</i>
    <h5>~15 Minutes for Delivery</h5>
  </div>
  <?php $conn = new mysqli($servername, $username, $password, $database);
  if ($conn->connect_error) {
    die("Connection to the DB failed: " . $conn->connect_error);
  } else { $stato = "Non confermato";
    $stmt = $conn->prepare("SELECT * FROM ordine WHERE cliente_email = ? AND stato = ?");
    $stmt -> bind_param("ss", $_SESSION["email"], $stato);
    $stmt->execute();
    $result = $stmt->get_result();
    if($result->num_rows > 0){
      $var = $result->num_rows;?>
      <div id="snackbar"><script>
      function myFunction() {
        var x = document.getElementById("snackbar");
        x.className = "show";
        setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
      }
      window.onload = myFunction();
      </script>You have <?php echo $var ?> new notifications</div>
    <?php }
  } ?>
  <div class="container" id="footer-homepage">
    <?php require 'footer.php'; ?>
  </div>

  <!-- Google Fonts API -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Staatliches">
</body>
</html>
