<nav class="navbar navbar-expand-md navbar-light bg-white sticky-top">
  <div class="container-fluid">
      <div class="nav-item">
        <span onclick="toggleNav()"><a class="navbar-menu"><i class="material-icons" style="color:black; cursor:pointer">menu</i></a></span>
      </div>

   <div class="navbar-header">
        <a class="navbar-brand-logo" href="/foodify-tecweb-project/index.php"><img src="/foodify-tecweb-project/img/logo.png"></a>
    </div>

    <div class="nav-item" style="margin-right: 10px;">
        <span onclick="toggleNav()"><a href="/foodify-tecweb-project/src/serving.php"><i class="material-icons" style="color:black; cursor:pointer">notifications</i></a></span>
    </div>


      <div class="nav-item">
        <span id="cart-button"><a class="navbar-cart-button"><i class="material-icons float:right" style="color:black; cursor:pointer">shopping_cart</i></a></span>
      </div>

  </div>
</nav>

<!-- Side Menu Settings -->
<div id="sidemenu" class="sidenav">
  <a href="/foodify-tecweb-project/src/logout.php">Logout</a>
</div>

<div id="sidecart" class="sidenav-cart">
  <p class="h4" id="label-cart" style="margin-top: 30px; margin-left: 10px; text-align: center">Shopping Cart</p><br/>
  <div class="card-group" id="cart-products" style="margin-right: 5px">
    <!-- Product will be putted here from sidecart.js -->
  </div>
  <div class="container-fluid">
    <div class="control-cart-btns row">
      <button type="button" id="delete-cart" class="btn btn-danger float-left" style="margin-left: 10px">Delete All</button>
      <button type="button" id="checkout-cart" class="btn btn-primary ml-auto" style="margin-right: 10px">Checkout</button>
    </div>
  </div>
</div>


<!-- JQuery -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>



<!-- Google's Material Design Icons -->
<link rel="stylesheet" href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Side Menu and Cart JS -->
<script src="/foodify-tecweb-project/js/sidemenu.js"></script>
<script src="/foodify-tecweb-project/js/sidecart.js"></script>
<link rel="stylesheet" href="/foodify-tecweb-project/css/navbar.css">
