<?php
//Dichiarazione variabili per server
session_start();
$servername="localhost";
$username ="root";
$password ="";
$database = "foodifydb";

$conn =new mysqli($servername, $username, $password, $database);
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <style>
  #snackbar {
    visibility: hidden;
    min-width: 250px;
    margin-left: -125px;
    background-color: #333;
    color: #fff;
    text-align: center;
    border-radius: 2px;
    padding: 16px;
    position: fixed;
    z-index: 1;
    left: 50%;
    bottom: 30px;
    font-size: 17px;
  }

  #snackbar.show {
    visibility: visible;
    -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
    animation: fadein 0.5s, fadeout 0.5s 2.5s;
  }

  @-webkit-keyframes fadein {
    from {bottom: 0; opacity: 0;}
    to {bottom: 30px; opacity: 1;}
  }

  @keyframes fadein {
    from {bottom: 0; opacity: 0;}
    to {bottom: 30px; opacity: 1;}
  }

  @-webkit-keyframes fadeout {
    from {bottom: 30px; opacity: 1;}
    to {bottom: 0; opacity: 0;}
  }

  @keyframes fadeout {
    from {bottom: 30px; opacity: 1;}
    to {bottom: 0; opacity: 0;}
  }
</style>
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<!-- Google's Material Design Icons -->
<link rel="stylesheet" href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

<link rel="stylesheet" href="/foodify-tecweb-project/css/ristoratore.css">
<script src="jquery-3.3.1.min.js"></script>

<title>RistoratoreHomePage</title>
</head>

<body>

  <!-- INSERIRE Navbar standard del progetto con LOGOUT??-->
  <div class="menu">
    <?php require 'restaurant-navbar.php'; ?>
  </div>


  <?php
  //Check della connessione al db se con successo o meno
  if ($conn->connect_errno) {
    die("Connection failed: " . $conn->connect_error);
  }
  ?>


  <div class="container">
    <nav>
      <div class="nav nav-tabs" id="nav-tab" role="tablist">
        <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Profile</a>
        <a class="nav-item nav-link" id="nav-ordini-tab" data-toggle="tab" href="#nav-ordini" role="tab" aria-controls="nav-ordini" aria-selected="false">Order</a>
        <a class="nav-item nav-link" id="nav-nuovoprodotto-tab" data-toggle="tab" href="#nav-nuovoprodotto" role="tab" aria-controls="nav-nuovoprodotto" aria-selected="false">New Product</a>
        <a class="nav-item nav-link" id="nav-listino-tab" data-toggle="tab" href="#nav-listino" role="tab" aria-controls="nav-listino" aria-selected="false">List</a>
      </div>
    </nav>
    <div class="tab-content" id="nav-tabContent">
      <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
        <div class="media">
          <img src="/foodify-tecweb-project/img/posate.jpg" class="align-self-start mr-3" alt="Bon apetit - from the Restaurant">
          <div class="media-body">
            <?php
            $query_sql = "SELECT nome, cognome, P_IVA, telefono, ristorante, email FROM fornitore WHERE email= ?";
            $stmt = $conn -> prepare($query_sql);
            $stmt->bind_param("s", $_COOKIE['user']);
            $stmt->execute();
            $result = $stmt->get_result();
            if($result->num_rows > 0){
              while($row = $result -> fetch_assoc()){
                ?>
                <h5 class="mt-0">Restaurant profile</h5>
                <div class="item">
                  <p class="item-name">Owner name</p>
                  <p class="item-description"><?php echo $row["nome"]; ?></p>
                  <hr>
                  <p class="item-name">Owner surname</p>
                  <p class="item-description"><?php echo $row["cognome"]; ?></p>
                  <hr>
                  <p class="item-name">Restaurant P_IVA</p>
                  <p class="item-description"><?php echo $row["P_IVA"]; ?></p>
                  <hr>
                  <p class="item-name">Restaurant telephone</p>
                  <p class="item-description"><?php echo $row["telefono"]; ?></p>
                  <hr>
                  <p class="item-name">Restaurant name</p>
                  <p class="item-description"><?php echo $row["ristorante"]; ?></p>
                  <hr>
                  <p class="item-name">Restaurant email</p>
                  <p class="item-description"><?php echo $row["email"]; ?></p>
                </div>
                <?php
              }
            }
            $stmt->close();
            ?>
          </div>
        </div>
      </div>


      <div class="tab-pane fade" id="nav-ordini" role="tabpanel" aria-labelledby="nav-ordini-tab">
        <?php
        $query_sql = "SELECT idOrdine, data, ora, prezzo_tot, cliente_email, stato FROM ordine";
        $result = $conn -> query($query_sql);
        ?>
        <table>
          <thead>
            <tr>
              <th scope="col">Id ordine</th>
              <th scope="col">Data</th>
              <th scope="col">Orario</th>
              <th scope="col">Prezzo</th>
              <th scope="col">Cliente</th>
              <th scope="col"></th>
              <th scope="col">Delivered</th>
            </tr>
          </thead>
          <tbody>
            <?php
            if($result -> num_rows > 0){
              while($row = $result -> fetch_assoc()){
                ?>
                <form action="/foodify-tecweb-project/controller/delete_order.php" method="post">
                  <tr>
                    <td><?php echo $row["idOrdine"]; ?></td>
                    <td><?php echo $row["data"]; ?></td>
                    <td><?php echo $row["ora"]; ?></td>
                    <td><?php echo $row["prezzo_tot"]; ?></td>
                    <td><?php echo $row["cliente_email"]; ?></td>
                    <td> <input type="hidden" name="idd" value="<?php echo $row["idOrdine"]; ?>"> </td>
                    <td> <button type="submit" class="btn btn-success">Delivered</button> </td>
                  </tr>
                </form>
                <?php
              }
            }
            ?>
          </tbody>
        </table>
      </div>


      <div class="tab-pane fade" id="nav-nuovoprodotto" role="tabpanel" aria-labelledby="nav-nuovoprodotto-tab">
        <form action="/foodify-tecweb-project/controller/insert_product.php" method="post">
          <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label">Imagine URL</label>
            <div class="col-sm-10">
              <input type="nome" class="form-control" id="inputimagine" placeholder="URL" name="immagine">
            </div>
          </div>
          <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label">List id</label>
            <div class="col-sm-10">
              <input type="number" class="form-control" id="inputidlistino" placeholder="list id" name="idListino">
            </div>
          </div>
          <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label">Product name</label>
            <div class="col-sm-10">
              <input type="nome" class="form-control" id="inputnome" placeholder="name" name="nome">
            </div>
          </div>
          <div class="form-group row">
            <label for="inputPassword3" class="col-sm-2 col-form-label">Product price ($)</label>
            <div class="col-sm-10">
              <input type="number" class="form-control" id="inputprezzo" placeholder="price" name="prezzo" step="0.01">
            </div>
          </div>
          <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label">Product type</label>
            <div class="col-sm-10">
              <input type="nome" class="form-control" id="inputtipo" placeholder="type" name="tipo">
            </div>
          </div>
          <div class="form-group">
            <label for="exampleFormControlTextarea1">Description</label>
            <textarea class="form-control" id="exampleFormControlTextarea1" rows="5" name="descrizione"></textarea>
          </div>
          <div class="form-group row">
            <div class="col-sm-10">
              <button type="submit" class="btn btn-primary">Insert</button>
            </div>
          </div>
        </form>
      </div>


      <div class="tab-pane fade" id="nav-listino" role="tabpanel" aria-labelledby="nav-listino-tab">
        <?php
        $query_sql = "SELECT idProdotto, nome, prezzo, tipo, descrizione, idListino FROM prodotto";
        $result = $conn -> query($query_sql);
        ?>
        <table>
          <thead>
            <tr>
              <th scope="col">Id Product</th>
              <th scope="col">Name product</th>
              <th scope="col">Price ($)</th>
              <th scope="col">Type</th>
              <th scope="col">Description</th>
              <th scope="col">Id List</th>
              <th scope="col"></th>
              <th scope="col">Delete</th>
            </tr>
          </thead>
          <tbody>
            <?php
            if($result -> num_rows > 0){
              while($row = $result -> fetch_assoc()){
                ?>
                <form action="/foodify-tecweb-project/controller/delete_product.php" method="post">
                  <tr>
                    <td><?php echo $row["idProdotto"]; ?></td>
                    <td><?php echo $row["nome"]; ?></td>
                    <td><?php echo $row["prezzo"]; ?></td>
                    <td><?php echo $row["tipo"]; ?></td>
                    <td><?php echo $row["descrizione"]; ?></td>
                    <td><?php echo $row["idListino"]; ?></td>
                    <td> <input type="hidden" name="id" value="<?php echo $row["idProdotto"]; ?>"> </td>
                    <td> <button type="submit" class="btn btn-outline-dark">Delete</button> </td>
                  </tr>
                </form>
                <?php
              }
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <?php $stato = "Non confermato";
  $stmt = $conn->prepare("SELECT * FROM ordine WHERE fornitore_email = ? AND stato = ?");
  $stmt -> bind_param("ss", $_SESSION["email"], $stato);
  $stmt->execute();
  $result = $stmt->get_result();
  if($result->num_rows > 0){
    $var = $result->num_rows;?>
    <div id="snackbar"><script>
    function myFunction() {
      var x = document.getElementById("snackbar");
      x.className = "show";
      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    }
    window.onload = myFunction();
    </script>You have <?php echo $var ?> new notifications</div>
  <?php } ?>
  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>

<style>

p.item-name{
  font-style: normal;
  text-decoration: underline;
  font-family: sans-serif;
  font-size: 16px;
  font-weight: bolder;
}

</style>
